import { Component } from '@angular/core';
import { TODO } from './models/todo.model';
import { TodoService } from './services/todoCRUD.service';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {

  constructor(private todoService:TodoService) {

  }

  title = 'todo-app';
  
  todos:TODO[] = [];

  newTodoValues:TODO = {
    label: '',
    username: '',
    description: '',
    done: false,
  };

  addTodo() {
    if(this.newTodoValues.label.length < 1 || this.newTodoValues.description.length < 30) {
      return alert('The label should have at least one character, and description should not be less than 30')
    }

    this.todoService.addTodo(this.newTodoValues);
    this.todos = this.todoService.getTodos();

    this.newTodoValues.label = '';
    this.newTodoValues.username = '';
    this.newTodoValues.description = '';
  }

  deleteTodo(todoLabel){
    this.todoService.deleteTodoItem(todoLabel); 
    this.todos = this.todoService.getTodos();
  }
  
  get done () {
    return this.todos.filter((t) => {
      return t.done
    })
  }
}

