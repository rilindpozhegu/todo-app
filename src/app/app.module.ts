import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { ListsComponent } from './lists/lists.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { FormsModule } from '@angular/forms';
import { TodoService } from './services/todoCRUD.service';

@NgModule({
    declarations: [
    AppComponent,
    ListsComponent,
    NavigationbarComponent, 
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})

export class AppModule { }
