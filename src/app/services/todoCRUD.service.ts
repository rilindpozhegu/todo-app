import { Injectable } from '@angular/core';
import { TODO } from './../models/todo.model';

@Injectable()
export class TodoService {

  constructor() { }


  todoList:TODO[] = [];


  getTodos():TODO[] {
    return this.todoList;
  }

  addTodo(newTodoValues:TODO) {
    const newTodoItem = {
        label: newTodoValues.label,
        username: newTodoValues.username,
        description: newTodoValues.description,
        done: newTodoValues.done,
      };
      this.todoList.push(newTodoItem);
  }  

  deleteTodoItem(todoLabel) {
    const newTodos = [];
    for(let t of this.todoList) {
      if(t.label !== todoLabel) {
        newTodos.push(t);
      }
    }

    this.todoList = newTodos;
  }

}